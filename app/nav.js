function gologin() {
    window.location.href = "/login.html"
}

async function logout() {

    const response = await fetch("/api/logout");
    if (response.status == 200) {
        window.location.href = "/login.html";
    } else {
        console.log(response);
    }
}

async function islogin() {

    const response = await fetch("/api/login");
    const parsedJson = await response.json();

    if (response.status == 403 || response.status == 422) {
        window.location.href = "/login.html";
    }

}

const pagename = document.currentScript.getAttribute("page");

let loginbut = document.getElementById("login-but");
loginbut.onclick = logout;

islogin();
