import os
import time

from webargs import fields

userpass = {
    "username": fields.Str(required=True), 
    "password": fields.Str(required=True)
}

logintoken = {
    "simpletodo-jwt": fields.Str(required=True)
}

adduser = {
    "username": fields.Str(required=True), 
    "password": fields.Str(required=True), 
    "admin": fields.Bool(missing=False)
}

deleteuser = {
    "ids": fields.List(fields.Int(required=True), required=True)
}

posttodos = {
    "data": fields.List(fields.Str(required=True), required=True)
}

deletetodos = {
    "data": fields.List(fields.Int(required=True), required=True)
}
