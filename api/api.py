import time
import os
import logging

from flask import Flask, jsonify, send_file
from flask_restful import Resource, Api, request
from flask_sqlalchemy import SQLAlchemy

from webargs import fields
from webargs.flaskparser import use_args

import auth
import responses
import wargs

app = Flask(__name__)
api = Api(app)

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get("POSTGRES_URL")

db = SQLAlchemy(app)


class User(db.Model):
    """User Table"""

    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(25), unique=True)
    password = db.Column(db.LargeBinary(32))
    salt = db.Column(db.LargeBinary(32))
    admin = db.Column(db.Boolean, default=False)
    todo_id = db.relationship("Todo", backref="user", lazy=True)

    def __repr__(self):
        return f"<User: {self.id} {self.username} Admin: {self.admin}>"

class Todo(db.Model):
    """Order Table"""

    __tablename__ = "todo"
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.BigInteger, default=int(time.time()))
    text = db.Column(db.String(4096))
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    def __repr__(self):
        return f"<Todo: {self.text[:32]}>"

    def serialize(self):
        return {
            "id": self.id,
            "date": self.date,
            "text": self.text,
            "user_id": self.user_id
        }

#db.create_all()
#db.session.commit()

#key, salt = auth.hash("password@123")
#new_user = User(username="admin", password=key, salt=salt, admin=True)
#db.session.add(new_user)
#db.session.commit()

@app.errorhandler(422)
def handle_422(err):
    return responses.unprocessableentity()

@app.errorhandler(500)
def handle_500(err):
    return responses.internalerror()


class Login(Resource):

    @use_args(wargs.logintoken, location="cookies")
    def get(self, cookies):
        """Test if user is logged in"""

        # valadate jwt
        try:
            data = auth.decode_jwt(cookies["simpletodo-jwt"])
        except:
            return responses.invalidtoken()

        return responses.login()


    @use_args(wargs.userpass, location="json")
    def post(self, args):

        db_user = User.query.filter_by(username=args["username"]).first()

        # chek if user exits in db
        if db_user == None:
            return responses.userpassincorrect()

        if db_user.password == auth.hash(args["password"], db_user.salt)[0]:
            # username and password are correct
            token = auth.encode_jwt(db_user.id, db_user.username, db_user.admin)
            return responses.logintoken(token)
        else:
            return responses.userpassincorrect()

class Logout(Resource):

    def get(self):
        return responses.logout()

class Users(Resource):

    @use_args(wargs.logintoken, location="cookies")
    def get(self, cookies):

        # valadate jwt
        try:
            data = auth.decode_jwt(cookies["simpletodo-jwt"])
        except:
            return responses.invalidtoken()
        
        # check if user has permission to list users
        if data["admin"] != True:
            return responses.insfperms()

        users = User.query.all()
        return responses.userlist(users)


    @use_args(wargs.logintoken, location="cookies")
    @use_args(wargs.adduser, location="json")
    def post(self, cookies, args):

        # valadate jwt
        try:
            data = auth.decode_jwt(cookies["simpletodo-jwt"])
        except:
            return responses.invalidtoken()
        
        # check if user has permission to add a user
        if data["admin"] != True:
            return responses.insfperms()

        key, salt = auth.hash(args["password"])
        new_user = User(username=args["username"], password=key, salt=salt, admin=args["admin"])
        db.session.add(new_user)

        try:
            db.session.commit()
        except Exception as e:
            e_msg = e.args[0]
            if 'duplicate key value violates unique constraint "user_username_key"' in e_msg:
                return responses.usertaken()
            else:
                raise e

        app.logger.info(f"New User: {new_user}")

        return responses.newuser()

    @use_args(wargs.logintoken, location="cookies")
    @use_args(wargs.deleteuser, location="json")
    def delete(self, cookies, args):
        
        # valadate jwt
        try:
            data = auth.decode_jwt(cookies["simpletodo-jwt"])
        except:
            return responses.invalidtoken()
        
        # check if user has permission to delete a user
        if data["admin"] != True:
            return responses.insfperms()

        # todo: check if user exists first
        for i in args["ids"]:
            User.query.filter((User.id==i)).delete()
        db.session.commit()

        return responses.deleteuser()


class TodoItem(Resource):

    @use_args(wargs.logintoken, location="cookies")
    def get(self, cookies):
        
        # valadate jwt
        try:
            token = auth.decode_jwt(cookies["simpletodo-jwt"])
        except:
            return responses.invalidtoken()

        items = Todo.query.filter_by(user_id=token["sub"]).order_by(Todo.date).all()

        return responses.todolist(items)


    @use_args(wargs.logintoken, location="cookies")
    @use_args(wargs.posttodos, location="json")
    def post(self, cookies, data):

        # valadate jwt
        try:
            token = auth.decode_jwt(cookies["simpletodo-jwt"])
        except:
            return responses.invalidtoken()

        for todoitem in data["data"]:
            new_todo = Todo(text=todoitem, user_id=token["sub"])
            db.session.add(new_todo)
        db.session.commit()

        return responses.newtodolist([new_todo,])

    @use_args(wargs.logintoken, location="cookies")
    @use_args(wargs.deletetodos, location="json")
    def delete(self, cookies, data):

        # valadate jwt
        try:
            token = auth.decode_jwt(cookies["simpletodo-jwt"])
        except:
            return responses.invalidtoken()

        Todo.query.filter((Todo.user_id==token["sub"]) & (Todo.id.in_(data["data"]))).delete()
        db.session.commit()

        return responses.deletetodo()


api.add_resource(Login, '/login')
api.add_resource(Users, '/users')
api.add_resource(Logout, '/logout')
api.add_resource(TodoItem, '/todo')

if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
